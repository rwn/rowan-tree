# xargs

[Manpage](https://www.mankier.com/1/xargs)

> Build and execute command lines from standard input

## Huh?

`xargs` takes a source of arguments and executes some command for all arguments.

## Example

### Conceptual

```bash

seq 5 | xargs echo

```

You can pass in -I for placeholder replacements

```bash

seq 5 | xargs -I _ echo "hi " _

```

### Practical

```bash

# pkglist is a newline-separated text file of package names to install
# argument '-a' tells xargs the input is from a file

/path/to/pkglist | xargs -a "$pkg_manager --install --yes-to-all"

```

---

## Useful URLs

- [Mankier](https://www.mankier.com/1/xargs)
